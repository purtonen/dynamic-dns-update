
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace DynamicDNSUpdate
{
    class BrowserController : IDisposable
    {
        BrowserControllerOptions options;
        ChromeDriver driver;
        public BrowserController(BrowserControllerOptions options)
        {
            this.options = options;
        }
        public void Run()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("headless");

            driver = new ChromeDriver(chromeOptions);
            driver.Manage().Window.Maximize();

            Login();
            NavigateToDNSSettings();
            SetAField();
        }

        void Login()
        {
            driver.Navigate().GoToUrl("https://www.domainhotelli.fi/oma");
            driver.FindElementById("inputEmail").SendKeys(options.Username);
            driver.FindElementById("inputPassword").SendKeys(options.Password);
            driver.FindElementById("login").Click();
        }

        void NavigateToDNSSettings()
        {
            driver.Navigate().GoToUrl($"https://www.domainhotelli.fi/clientarea.php?action=productdetails&id={options.Id}&dosinglesignon=1");
            driver.FindElementById("item_zone_editor").Click();

            var manageButtonId = $"manage_for_{options.Domain.Replace('.', '_')}";

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementExists(By.Id(manageButtonId)));

            driver.FindElementById(manageButtonId).Click();
        }

        void SetAField()
        {
            // Try modifying the existing A field
            // If no current A field for domain is present, create a new one
            try
            {
                ModifyAField();
            }
            catch
            {
                CreateAField();
            }
        }

        void ModifyAField()
        {
            // Well yes, this is nice. 
            // Find a <tr> element that has the Domain in 
            // the name field and A in the field field (yes, it is the field field)
            var rowXPath = $"//tr[child::td/span[starts-with(text(), '{options.Domain}')] and child::td/span[text()='A']]";

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(rowXPath)));

            var row = driver.FindElementByXPath(rowXPath);
            row.FindElement(By.XPath(".//button[contains(@id, edit_btn)]")).Click();

            PopulateAndSaveForm();
        }

        void CreateAField()
        {
            driver.FindElementById("search_add_record_btn").Click();
            PopulateAndSaveForm();
        }

        void PopulateAndSaveForm()
        {
            // Clicking the edit button creates a new row so we need to fetch it
            var row = driver.FindElementById("addRecordForm");
            var addressField = row.FindElement(By.XPath(".//input[@id='record_a_address']"));
            addressField.Clear();
            addressField.SendKeys(options.NewIPAddress);

            row.FindElement(By.XPath(".//button[@id='inline_edit_record_button']")).Click();

            var rowXPath = $"//tr[child::td/span[starts-with(text(), '{options.Domain}')] and child::td/span[text()='A']]";
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(rowXPath)));
        }

        public void Dispose()
        {
            driver.Close();
            driver.Quit();
        }
    }
}
