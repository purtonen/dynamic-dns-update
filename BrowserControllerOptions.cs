
using System;

class BrowserControllerOptions
{
    public string Username { get; set; }
    public string Password { get; set; }
    public string Id { get; set; }
    public string Domain { get; set; }
    public string NewIPAddress { get; set; }
    public BrowserControllerOptions()
    {

    }
    public BrowserControllerOptions(string username, string password, string id, string domain, string newIPAddress)
    {
        Username = username;
        Password = password;
        Id = id;
        Domain = domain;
        NewIPAddress = newIPAddress;
    }

    public void LoadEnvironmentVariables()
    {
        Username = Environment.GetEnvironmentVariable("DYNAMIC_DNS_USERNAME");
        Password = Environment.GetEnvironmentVariable("DYNAMIC_DNS_PASSWORD");
        Id = Environment.GetEnvironmentVariable("DYNAMIC_DNS_ID");
        Domain = Environment.GetEnvironmentVariable("DYNAMIC_DNS_DOMAIN");
    }
}