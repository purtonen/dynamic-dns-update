﻿
using System.Net;
using dotenv.net;

namespace DynamicDNSUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            DotEnv.Config(true);

            var options = new BrowserControllerOptions();
            options.LoadEnvironmentVariables();
            options.NewIPAddress = new WebClient().DownloadString("http://icanhazip.com").Trim();

            using (var controller = new BrowserController(options))
            {
                controller.Run();
            }
        }
    }
}
