# Dynamic DNS Update

## Premise

Finnish ISP's do not provide static IP addresses for personal accounts, only for companies. 
So we need to keep updating the dynamically changing IP address for a domain service. 

Domainhotelli domain service does not provide any way to programmatically update DNS A fields. 
So we need to go through their website UI to change said A fields. 

## Solution

1. Fetch public IP address from icanhazip.com
2. Use Selenium to run a headless browser instance and update the IP to domainhotelli.fi

